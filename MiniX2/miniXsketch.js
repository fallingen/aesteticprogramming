//getting the variables out the way
let wordle,s,button,day,autton,cutton,dutton,eutton,futton,gutton,hutton,iutton,cb;

function setup() {
createCanvas(windowWidth,windowHeight)
background(100)
push()
   wordle = createInput();
    wordle.position(windowWidth/2-50,windowHeight/2);
    wordle.size(100,25);
    wordle.input(myInputEvent);
    button = createButton('Submit Wordle');
      button.position((windowWidth/2)-50,(windowHeight/2)-50);
      button.mousePressed(loadMoodle);
      textAlign(CENTER)
      textSize(100)
      text('Moodle Board',windowWidth/2,100)
      textSize(30)
      text('Paste your Wordle, and get your Moodle!',windowWidth/2,200)
      pop()
      //creating the Button, input field, and creating the names for the functions below, like loadMoodle and myInputEvent
}



function myInputEvent() {
  s = wordle.value();//this creates a string with the Wordle Share in it

//Things below is what i used to test if what i did worked
//createP(wordle.value());
//print(wordle.value());
//print(s.substring(7,14));
}
function loadMoodle(){
day= s.substring(7,14);//this get's the daily wordle numbers and attemps from the Wordle share input
resizeCanvas(windowWidth, windowHeight+300);
background(100);

push()
textAlign(CENTER)
textSize(100)
text('Moodle Board',windowWidth/2,100)
textSize(30)
text('Paste your Wordle, and get your Moodle!',windowWidth/2,200)
pop()

//the rest is just the 8 buttons emoji, texts both for what get's displayed and copied
autton=createButton('Share Moodle');
cutton=createButton('Share Moodle');
dutton=createButton('Share Moodle');
eutton=createButton('Share Moodle');
futton=createButton('Share Moodle');
gutton=createButton('Share Moodle');
hutton=createButton('Share Moodle');
iutton=createButton('Share Moodle');

autton.position(windowWidth*0.15,windowHeight);
cutton.position(windowWidth*0.35,windowHeight);
dutton.position(windowWidth*0.55,windowHeight);
eutton.position(windowWidth*0.75,windowHeight);
futton.position(windowWidth*0.15,windowHeight+200);
gutton.position(windowWidth*0.35,windowHeight+200);
hutton.position(windowWidth*0.55,windowHeight+200);
iutton.position(windowWidth*0.75,windowHeight+200);

autton.mousePressed(copyMoodlea);
cutton.mousePressed(copyMoodlec);
dutton.mousePressed(copyMoodled);
eutton.mousePressed(copyMoodlee);
futton.mousePressed(copyMoodlef);
gutton.mousePressed(copyMoodleg);
hutton.mousePressed(copyMoodleh);
iutton.mousePressed(copyMoodlei);
textSize(14.2)
text('\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n⬛🟨🟨🟨⬛\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨',windowWidth*0.15,windowHeight-155)
text('\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n🟨🟨🟨🟨🟨\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨',windowWidth*0.35,windowHeight-155)
text('\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n🟨⬛⬛⬛🟨\n⬛🟨🟨🟨⬛\n🟨🟨🟨🟨🟨',windowWidth*0.55,windowHeight-155)
text('\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟦🟨🟦🟨\n🟨⬛⬛⬛🟨\n⬛🟦🟨🟦⬛\n🟨🟦🟨🟦🟨',windowWidth*0.75,windowHeight-155)
text('\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n⬛⬜⬜⬜⬛\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨',windowWidth*0.15,windowHeight+55)
text('\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟦🟨\n⬛🟨🟨🟨⬛\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨',windowWidth*0.35,windowHeight+55)
text('\n\n🟨🟨🟨🟨🟨\n🟨⬛⬛⬛🟨\n⬛🟨🟨🟨⬛\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨',windowWidth*0.55,windowHeight+55)
text('\n\n🟥🟥🟥🟥🟥\n⬛⬛🟥⬛⬛\n🟥⬛🟥⬛🟥\n🟥🟥🟥🟥🟥\n🟥⬛⬛⬛🟥\n🟥🟥🟥🟥🟥',windowWidth*0.75,windowHeight+55)
}

//this is the way to easily copy something to someones clipboard and a text pop up
function copyMoodlea(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n⬛🟨🟨🟨⬛\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨')
  window.alert('Moodle copied');
}
function copyMoodlec(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n🟨🟨🟨🟨🟨\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨')
window.alert('Moodle copied');
}
function copyMoodled(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n🟨⬛⬛⬛🟨\n⬛🟨🟨🟨⬛\n🟨🟨🟨🟨🟨')
window.alert('Moodle copied');
}
function copyMoodlee(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟦🟨🟦🟨\n🟨⬛⬛⬛🟨\n⬛🟦🟨🟦⬛\n🟨🟦🟨🟦🟨')
window.alert('Moodle copied');
}
function copyMoodlef(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨\n⬛⬜⬜⬜⬛\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨')
window.alert('Moodle copied');
}
function copyMoodleg(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟦🟨\n⬛🟨🟨🟨⬛\n🟨⬛⬛⬛🟨\n🟨🟨🟨🟨🟨')
window.alert('Moodle copied');
}
function copyMoodleh(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟨🟨🟨🟨🟨\n🟨⬛⬛⬛🟨\n⬛🟨🟨🟨⬛\n🟨🟨🟨🟨🟨\n🟨⬛🟨⬛🟨\n🟨🟨🟨🟨🟨')
window.alert('Moodle copied');
}
function copyMoodlei(){
  cb=navigator.clipboard;
  cb.writeText('Moodle '+day+'\n\n🟥🟥🟥🟥🟥\n⬛⬛🟥⬛⬛\n🟥⬛🟥⬛🟥\n🟥🟥🟥🟥🟥\n🟥⬛⬛⬛🟥\n🟥🟥🟥🟥🟥')
window.alert('Moodle copied');
}

## MiniX2 submission
Moodle Board

-----

### Screen shots, executable link and code link  


Screenshot:<br>
https://gitlab.com/fallingen/aesteticprogramming/-/blob/main/MiniX2/Moodle_board.png

<br>

Executable program :
https://fallingen.gitlab.io/aesteticprogramming/MiniX2/

<br>

code repository :
https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX2


 -----

 ### Thoughts
 <br>
 What i have created is a "mood board" where you can put in the Wordle share text and emojies and select between 8 emojis recreated in the Wordle square emoji form as some sort of pixel art.
 This emoji will get copied to your clipboard along with some text in the familiar Wordle format.     
 <br>

 I searched all over the internet for the syntaxes i need to make this happen. The most challenging part was to figure out exactly how the program would recognize the Wordle input and how I could use that to do what i'd want. This took me a long time, but in the end I got it to work by putting the input into a string and using the substring syntax. The reason it was challenging was because most video would show you how to do set it up in HTML, CSS and Javascript, which not only would just increase the coding language i would need to understand, but would also make my code (even) more confusing to figure by my fellow student. I eventually figured out how to it all within the .js file, which i was happy about. Whether it's "good" coding practice is another question entirely.    
 <br>

 The reason for me to create this board i because i for some time now have played Wordle daily, and im in a groupchat(Discord server) where many people also share their Wordle daily. The regular Wordle share is brilliant in many ways, but where is lacks is to show how the player feels about a particular Wordle. Some people might be happy with a 5/6 while other might be mad about a 3/6. This probably varies due to many factors such as: time spent on the Wordle, what you perceive your skill to be, whether you were really close in a/many previous attemp(s), whether you are directly competing with another person etc. Initially i wanted my program to try and guess your Moodle base on a few factors, but in the end i just showed every option, because everyone can have such varied experience with any given Wordle, that i felt it was more inclusive to just let people select their Moodle.    
 <br>

 Initially i wanted my program to try and guess your Moodle base on a few factors, but in the end i just showed every option, because everyone can have such varied experience with any given Wordle, that i felt it was more inclusive to just let people select their Moodle.
 <br>



#### Reflection on how my program fits into a political, social and cultural context.

the emoji part itself is a pixel art version of some of the more basic Unicode emojies. I wanted the pixel art to fit within the original maximum rid of 5x6 and this limitation both made i difficult to recreate a lot of the unicode emojies, but the ones i have managed to create works well enough, and the formatting being the exact same allows the 8 emojies to be understood in a greater cultural understanding rather than "just" pixel art emojies.     

## MiniX3 submission
Take a breather

-----

### Screen shots, executable link and code link  


Screenshot:<br>
https://gitlab.com/fallingen/aesteticprogramming/-/blob/main/MiniX3/minix3_breather.png

<br>

Executable program :
https://fallingen.gitlab.io/aesteticprogramming/MiniX3/

<br>

code repository :
https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX3


 -----

 ### Thoughts
 <br>
I wanted to explore a very human feeling "time experience" like breathing with my throbber. I wanted to try an combine the machine code way of telling time, with the a very human way of perceiving time. i also wanted to keep it simple, short and sweet, as i''m very bad at that apparently  
This i did with the scale() syntax and a Boolean expression about when to "breath in" and "breath out". All this is controlled by the frameRate syntax that controls how often draw gets called. In some sense the Boolean expression is an internal metronome with the p and s variable swinging back and forward. "What runs, but goes no where?" This Boolean expression i guess. Now of course this doesn't actually tell any "real" information about how the loading is going. You could perhaps, if you had any data aobut how a load was going, you could tie it to different things, like the rate of breathing, either the total load% or the current rate of loading, but either way it still would hide how far is left, because there would be no way to tell, just guesses.   
 <br>
A Throbber like Facebook's feed loading throbber can be seen to be similar to mine. It "breaths" through a grey scale and appears as a 'yet to load post' where you have just a few of the layout features being there pulsing, like the profile picture, the when it was posted line and the like comment and share button. Thought this is clearly just a throbber based on nothing, as the post that loads it it's place could be longer that the gray scale throbber suggested, or it could even be friend recommendations or something like that. Therefor Facebook's throbber does 'lie' about what's about to load. But what this 'lie' does do, it create a for seemless scrolling experience, as you probably won't notice the 'lie' of the throbber, as you scroll your feed. On the other hand, something like the basic Youtube loading spinny circle line throbber thing feels like you have hit a brick wall of what you were doing and the throbbers existence probably feels so much worse to the user. And Youtubes throbber then also doesn't so a good job at actually conveying loading info, so it still tells a 'lie' without getting a benefit nearly as big as Facebook.       

let r,g,b;
let s,p;//s is the scale, p is the variable that pings the lines.
function setup() {
createCanvas(windowWidth,windowHeight);
background(255);
frameRate(30)
s=0.2
p=0.005
//random start colour
r=random(0,255);
g=random(0,255);
b=random(0,255);
}


function draw() {
  background(255)
  textSize(50)
  let t = text('Take a breather while we wait',windowWidth/2-350,50)
  push()
  scale(s)
    translate(windowWidth/2/s,windowHeight/2/s)

  for (i=0;i<359;i++){
    rotate(i)
    stroke(r,g,b)
    strokeWeight(3)
    line(0,0,200,200)
  }

s=s+p
if(s>1){
  p=-p
  r=random(0,255);
  g=random(0,255);
  b=random(0,255);
} else if(s<0.1){
  p=-p
  r=random(0,255);
  g=random(0,255);
  b=random(0,255);
}
pop()

}

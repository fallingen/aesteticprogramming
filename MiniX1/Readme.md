## MiniX1 submission
The random spider generator

-----

### Screen shots, executable link and code link  

TW, Arachnophobia

Screenshot:<br>
https://gitlab.com/fallingen/aesteticprogramming/-/blob/main/MiniX1/spider_click.png

<br>

Executable program, click your own spider :
https://fallingen.gitlab.io/aesteticprogramming/MiniX1/

<br>

code repository :
https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX1


 -----

 ### Thoughts
 <br>
 What I have made is a "random spider generator".
 After I had made the initial circle, I wanted to expand an idea out that circle to try and create something grander. What I ended up with wanting to so was a spider, because the black circle I had made kinda reminded me of a spider body (even though I don't really like spiders in real life at all)
 <br>

 So I went looking at the p5.js website for some references that I could use to create a better spider drawing. I also ended up wacthing a good bit of The Coding Train's play list on p5.js for some ideas( https://www.youtube.com/watch?v=HerCR8bw_GE&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA ) This gave me good knowledge about many different syntaxes and other stuff about p5.js like execution order etc.
 <br>

 But I didn't really find what I needed to make the legs for the spider. I already had the idea to use some sort of curve, but the different ways to draw curves in p5.js confused me at first. I didn't understand why it needed so many inputs, and I couldn't figure out exactly what changing the numbers did exactly.
 <br>

 So i went on a multi day(granted not all day) search in order to learn about curves in js.
 Ended about learning how bezier curves worked, and went to on drawing some leg on that looked good.
 <br>

 During this research, I also thought about making the spiders random, as I had also seen that in a The Coding Train video. When I had draw some legs on the spider that I liked, I started moving with the parameters in the bezier curve legs to try and find boundaries with what looked good/interesting in my eyes. Took a lot of tinkering, but I found out the numbers and ratios that i liked for the spider legs. I then set of to write this down as code, and other than spelling random wrong once, my code worked the first time, which was very rewarding :).
 I initially wanted a spider web as well, so you could create a scenic screenshot with spider on the web, but I ended up not making it, both because i again didn't know how and would have to take more time out to figure that out, but also I had made so many variables that just spamming spiders on the canvas to check out how different all the spiders could be was quite fun on it's own. So I just stuck with that concept.
 <br>

#### Reflection on how this first (for me) coding experience has been in relation to writing and reading

Languages like Danish or English are very useful tools when you want to communicate anything and ,like every human languages, can be used in different ways to communicate with each other on many different levels. Friend groups or families can have so much internal co-understanding that you often can communicate much deeper info through very simple language. But sometimes like writing assignments or emails to your landlord, you can tab into a deeper understanding of languages, in order to convey your thoughts more accurately, whether it's for formal or informational reasons.
But this can sometimes be difficult for me to do, either with my thoughts not converting well into written words, as I think in spoken word, but also sometimes my thoughts don't convert well into a specific language.
<br>

Like "human" languages, code languages are still tools you can use to communicate your thoughts to others. And for me when i have and to write something grander like an exam, the experience coding for the first time was a lot like that.
A large period of reading, reflection and learning, often including writers blocks, copious amounts of stress, followed by flood gates opening, where I'm able to write down/code what I've been prepping in my mind.   

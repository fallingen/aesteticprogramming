
let ox,oy,ix,iy,c,r,g,b,s;
//variables for the outer x and y, inner x and y, c for "curve" of the legs
//and rgb for random colour and s foe scary big spider

function setup() {
createCanvas(windowWidth,windowHeight);
background(100);
angleMode(DEGREES)
textSize(100);
textStyle(NORMAL);
text('TW: Arachnophobia',100,150)
textSize(50);
text('left click: Random Spider',200,250)
text('right click: clear canvas',200,350)
}

function draw(){

//this is the clear function
if(mouseIsPressed=== true){
if(mouseButton===RIGHT){
  background(100)
}
}
}

function mousePressed(){
  if(mouseButton===LEFT){

//spider leg randomizer
  ox = (random(40,80)+random(40,80))/2;
  oy = ox*(random(0.45,0.80)+random(0.45,0.80))/2;
  ix = ox*(random(0.35,0.80)+random(0.35,0.80))/2;
  iy = oy*(random(1.1,2)+random(1.1,2))/2;
  c = random(0,iy);

//random colour code
  r = random(0,255);
  g = random(0,255);
  b = random(0,255);

//rare giant spider variable
if (random(0,1000)<1) {
  s = 8
} else {
  s = 0
}

// the random spider colour and leg code
  noFill()
  stroke(r,g,b);
  strokeWeight(3);
  translate(mouseX,mouseY);
  rotate(random(0,359));
  scale((random(0.1,1)+random(0.1,1)/2)+s);
  bezier(-ox,oy,0,c,0,-c,-ox,-oy);
  bezier(-ix,iy,0,c,0,-c,-ix,-iy);
  bezier(ox,oy,0,c,0,-c,ox,-oy);
  bezier(ix,iy,0,c,0,-c,ix,-iy);

//the spider body code
  fill(r,g,b);
  noStroke();
  ellipse(0,0,60,40);
  ellipse(40,0,30,30);

}
}

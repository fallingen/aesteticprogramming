## MiniX4 submission
You(rdata)

-----

### Screen shots, executable link and code link  


Screenshot:<br>
https://gitlab.com/fallingen/aesteticprogramming/-/blob/main/MiniX4/minix4.png

<br>

Executable program :
https://fallingen.gitlab.io/aesteticprogramming/MiniX4/

<br>

code repository :
https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX4


 -----

 ### You(rdata)
 <br>
  -Provide: The idea behind this program is for said program to tell you exactly what data is has collected about you, but in a more direct humanistic way. The idea was also for the things the program would tell it knew about you, that those things would be something that seems like it could have come from actual data collection possibility/reality. This obviously is not actually true, as the only things that get's collected are the some text strings and mouse positions.
 <br>
 <br>
 -Describe: the program is itself just logging mouse positions and text strings, from varies mouse interaction. There's just the mouse being on canvas, there's the mouse being pressed and there's the mouse being pressed near the weird figures. I used a combination of bezier curves, ellipses and lines to create the figures, as well as push() and pop() to contain the drawing variables to each drawn element.       
 <br>
 <br>
 -Articulate: This program critiques the "capture all" theme by relaying back the "data capture" in a more humanistic way. This is meant as a thing to frighten you a little bit. Because even though all the data captures that were hinted at in the sting text are technically plausible, or more often than not reality, you's still be frighten if any human, even your mom, came to you and knew all of this, let alone entire companies knowing about it.
 <br>
 The program is also a perhaps meta critique of the datafication of life itself, trying to collect as much data, to then use that to reverse engineer life itself threw said data. The reality is however that life isn't "codable" and all attempt to do this simplistic life to code data transformation on a serious thing, will end up discriminating against minorities and lower classes, whether that's intentional(Like Amazon's 'time of task') or not.   
 <br>
 <br>
 The cultural implication of this "capture all" theme can become massive. This to me has similar "vibes" to one of the origins of Participatory design. in the 1970'ies in Norway the Iron and Metal workers union wanted to fight against the ways in which computers were being implemented in the workplace. The workers felt like the computers were implemented in a way that devalued their labour, and only was implemented in ways that their bosses would benefit from. This i think has some parallel to "capture all" where the data capture still pretty much just happens for corporations profit reasons. It is also impossible to know what fake "real life" someone can end up creating out of this data, where your or anyones life can end up getting devalued because of some computer code that didn't exactly match reality, like credit score systems in China or the USA.       

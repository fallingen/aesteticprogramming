
let xr,yr;
let q=0//i use this q to place the corn objects in the popcorn array

//my corn class
class Corn{
  constructor(x,y,r,w){
    this.x=x;
    this.y=y;
    this.r=r;
    this.w=w;

}

//this draws the corn kernels
harvest(){
  push()

let colorS=color(200,125,0);
let colorE=color(255);

//this is a gradiant colouring thing, it just worked
let gradient = drawingContext.createLinearGradient(
  0,10,0,30
);
gradient.addColorStop(0, colorS);
gradient.addColorStop(1, colorE);
drawingContext.fillStyle = gradient;

  noStroke();
translate(this.x,this.y)
rotate(this.r)

ellipse(0,0,35,40);
 bezier(0,30,-25,5,-25,-12.5,0,-20);
 bezier(0,30,25,5,25,-12.5,0,-20);

 pop()

 }

//this checks if the ehat plate is near the center of the corn kernels
 clicked(px,py,i){
 let d=dist(px,py,this.x,this.y);
 if (d<100){
   corn[i].shake(i);

 }
 }

//this shakes the corn kernels
 shake(i){
   this.x=this.x + random(-5,5)
   this.y=this.y + random(-5,5)
   this.w++


//the this.w is a value assigned to the object to count up when 'heat' is apllied, so when the this.w value hits 51, it gets sent to the popcorn array instead
if (this.w>50){
  xr=random(-15,15)
  yr=random(-15,15)
  popcorn[q] = new Popcorn(this.x,this.y,this.r,this.w,xr,yr)
  corn.splice(i,1)
  q++
}
}

}


//function poof(){
//corn.splice(p,1)
//   }

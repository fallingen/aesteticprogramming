## MiniX6 submission
Moodle Board

-----

### Screen shots, executable link and code link  


Screenshot of program after a little playing<br>
https://gitlab.com/fallingen/aesteticprogramming/-/blob/main/MiniX6/popcorngame.png

<br>

Executable program
https://fallingen.gitlab.io/aesteticprogramming/MiniX6/

<br>

code repository :
https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX6


 -----

 ### How the game works
 <br>
  The game it quite simple, you use your left click to create a heating area around the mouse, then when the corn kernels that have spawned have spent enough time in the heat, they "pop" and out flies a popcorn adding 1 to your score.
  and there's a button to continue getting more kernel, so it's kinda like a slightly more manual and slower cookie clicker
 <br>
the way the program works is that, first the 25 corn kernel objects get's created and put into an array. These corn objects carry with them some values, a random x and y for where to draw them, and a random r for how much to rotate them. Then there's the heat plate, which is just a checker if the mouse is pressed and is near a x and y of a given corn kernel in the array. If one is, it's starts to vibrate and shortly after is will burst and send a popcorn flying. This is done by having another class of objects called Popcorn that inherits the x and y values from the corn kernels that had spent enough time in the heat. The popcorn object is also given the random direction values that are need to make them, move across and out of the screen. The Basic concept is that some cordinates gets send around in order to translate objects arounf the 'this.' values, and try and use those to have the object "act" like they would in real life.  

 <br>
 ### objects abstractions and thoughts
<br>
When i figures out i wanted to make popcorn, the abstracted attributes that wanted to try and apply to the objects was the vibration of the corn, the heat+time=pop, and the wild flying nature of a popcorn popping. There's some more thing that i could have added like gravity and the kernels cooling down(right now if heat is applied, it never goes down again) and a popping sounds etc., but i was satisfied with this level of abstraction. But even with these attributes that i specificaally made for corn kernels and popcorn, is doesn't wake much change of how the background code works, to create something completely different. Like i can imagine that you could use this code to make an abstraction for fireworks. Spawn a lot in, use the proximity code to lit the fireworks, with the array splicing to create the explosion rather than popcorn, move the moving part to the corn object and redraw the thing and there you are, nearly the same abstraction for 2 different things. it's the classic "Describe a chair; a usually 4 legged thing that a human can sit on; so a horse=chair?". It seems it's almost impossible to describe something with simple abstraction, and it seems pointless and impossible to try and create every possible exception and physics simulation as code. In the same vein this thinking that concepts can easily be abstracted. is also a fallacy that some ,usually cis, people do when it comes to gender, where they want gender be something "simple" and "easy to abstract" either by saying. Now anyone can of course figure out that gender is much more complicated than that, but some people actively want to create the idea that 'it is' that simple. This kinda links to the class system in the object, as if you in society can create a 'class' of people based on simplistic criteria, therefore creating a (non)class of people that's outside of that class, you can much more easily discriminate against that (non)class, because if they aren't in the class/currect class, there won't be created anything for them                
 <br>
 helpful sources was very much watching Daniel Shiffman's entire 6.1-7.8 p5.js videos https://www.youtube.com/watch?v=xG2Vbnv0wvg&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=23
 as well as this video from Kazuki Umeda for the colour gradient help https://www.youtube.com/watch?v=-MUOweQ6wac


class Popcorn{
  constructor(x,y,r,w,xspeed,yspeed){
    this.x=x;
    this.y=y;
    this.r=r;
    this.w=w;
    this.xr=xspeed;
    this.yr=yspeed;

}

//here the popcorn get's drawn
explode(){
push()
translate(this.x,this.y)
rotate(this.r)
noStroke()
fill(255)
bezier(0,0,-50,-100,50,-100,0,0)
bezier(0,0,0,-150,100,150,0,0)
bezier(0,0,75,50,-50,150,0,0)
bezier(0,0,-100,-25,0,100,0,0)
bezier(0,0,-50,-100,-100,0,0,0)

pop()
push()
translate(this.x,this.y)
rotate(this.r-40)
noStroke()
fill(225,125,0)
ellipse(-10,-30,20,30)
ellipse(0,-10,30,20)
ellipse(-15,10,20,30)
ellipse(-29,-10,30,20)
pop()

//here the popcorn get's sent in a direction, the Boolean statement is so they don't continue into infinity
if(this.x<windowWidth+500 && this.x>-500 && this.y<windowHeight+500 && this.y>-500){
this.x=this.x+this.xr
this.y=this.y+this.yr
}
}




}

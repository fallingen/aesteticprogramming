let corn,popcorn; //declar my variales
let xi,yi,ri,wi;

function setup(){
  createCanvas(windowWidth,windowHeight-50);
angleMode(DEGREES)
frameRate(60)
popcorn=[];

resetSketch()//a function that runs on setup and every time you press the button, it give more corn kernels

let button=createButton('Get more kernels')
button.position(windowWidth/2,windowHeight-45)
button.size(150,40)
button.mousePressed(resetSketch);
  }



function draw(){
  background(100);
  for (let i =corn.length-1;0<=i;i--){
  corn[i].harvest();
}
  if (mouseIsPressed === true) {
    for (let i =corn.length-1;0<=i;i--){
      corn[i].clicked(mouseX,mouseY,i)
    }
}
for(let o=0;o<popcorn.length;o++){
  popcorn[o].explode()
}
//the above 3 for loops are for the object drawing in the other files

// this is the heat plate spawner
if (mouseIsPressed === true) {
for (let i=4;0<=i;i--){
push()
  noStroke()
  fill(255,200-(i*45),0,50)
  ellipse(mouseX,mouseY,50+25*i)
  pop()
}
}
textSize(25)
text('Total popcorns popped:',10,25)
text(popcorn.length,275,25)
text('Left click for heat',10,70)
}


//the function that creates a new corn array, once at start up and once every time the button is pressed
function resetSketch(){
  corn=[];

for (let i=0;i<30;i++){
  xi= random(75,windowWidth-75);
  yi= random(75,windowHeight-125);
  ri= random(0,359);
  wi=0
  corn[i] = new Corn(xi,yi,ri,wi);

 }
}

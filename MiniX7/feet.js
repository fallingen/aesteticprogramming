//Feet 1
class Feet{
constructor(a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,mr1,mg1,mb1,n1,o1,q1,t1,u1,v1,w1,x1,y1){
  this.a1=a1
  this.b1=b1
  this.c1=c1
  this.d1=d1
  this.e1=e1
  this.f1=f1
  this.g1=g1
  this.h1=h1
  this.i1=i1
  this.j1=j1
  this.k1=k1
  this.l1=l1
  this.mr1=mr1
  this.mg1=mg1
  this.mb1=mb1
  this.n1=n1
  this.o1=o1
  this.q1=q1
  this.t1=t1
  this.u1=u1
  this.v1=v1
  this.w1=w1
  this.x1=x1
  this.y1=y1

}

feetDraw(){
  if(this.x1>windowWidth+100){
    this.n1=random(-10,-5);
    this.h1=0;
    this.mr1=random(0,255);
    this.mg1=random(0,255);
    this.mb1=random(0,255);
  } else if(this.x1<-100){
    this.n1=0;
    this.h1=random(5,10);
    this.mr1=random(0,255);
    this.mg1=random(0,255);
    this.mb1=random(0,255);
  }

  if(this.y1>windowHeight+100){
    this.k1=random(-10,-5);
    this.l1=0;
    this.mr1=random(0,255);
    this.mg1=random(0,255);
    this.mb1=random(0,255);
  } else if(this.y1<-100){
    this.k1=0;
    this.l1=random(5,10);
    this.mr1=random(0,255);
    this.mg1=random(0,255);
    this.mb1=random(0,255);
  }

//this section adds a modifier so the feet gets drawn more consistently infront in the direction they are going
if(this.v1.heading()<0){
 this.e1=0
 this.f1=10
}else if(this.v1.heading()>0){
  this.e1=10
  this.f1=0
}


//mapping the noise to something more useful that flots from 0 to 1
this.q1=(this.e1+8)*map(noise(this.t1),0,1,this.n1,this.h1);
this.w1=(this.f1+8)*map(noise(this.u1),0,1,this.k1,this.l1);

//variable setup for drawing the feet, Note that g1 and o1 get defined by x1 an y1, before they are diffined themselves, this means that i have 2 different sets of points stored and create the direction the feet move in later
this.g1=this.x1-this.e1
this.o1=this.y1-this.f1
this.x1=this.x1+this.q1;
this.y1=this.y1+this.w1;
this.t1+=0.1;
this.u1+=0.1;

//here is use the 2 points to create a vector

this.v1=createVector(this.x1-this.g1,this.y1-this.o1)
this.j1=dist(0,0,this.x1-this.g1,this.y1-this.o1)

//this next part is a very math solution to get 2 point that are wide of a center point, and are defined in a way that allows me to use a canstantly moving translate() to draw to the feet like this,
 this.a1=(2*this.j1)*cos(this.v1.heading())
 this.b1=(2*this.j1)*sin(this.v1.heading())
 this.c1=createVector(this.a1,this.b1)

//the feet drawing
push()
noStroke(0)
fill(this.mr1,this.mg1,this.mb1)
angleMode(DEGREES)
translate(this.g1+this.c1.x/2,this.o1+this.c1.y/2)
rotate(this.v1.heading())
this.d1=this.d1*-1

//the feet themselves
bezier(0,this.d1*this.j1,0,this.d1*this.j1+37,55,this.d1*this.j1,75,this.d1*this.j1)
bezier(0,this.d1*this.j1,0,-37+this.d1*this.j1,55,this.d1*this.j1,75,this.d1*this.j1)
bezier(15,this.d1*this.j1,37,18+this.d1*this.j1,130,37+this.d1*this.j1,130,this.d1*this.j1)
bezier(15,this.d1*this.j1,37,-18+this.d1*this.j1,130,-37+this.d1*this.j1,130,this.d1*this.j1)
pop()
}
}



let a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,mr1,mg1,mb1,n1,o1,q1,t1,u1,v1,w1,x1,y1;
let feet;
let f,p;
let slider;
function setup() {
  createCanvas(windowWidth, windowHeight-50);
  background(0);
  frameRate(4);
  angleMode(DEGREES)
  slider = createSlider(1,20,8);
  slider.position((windowWidth/2)+100,windowHeight-35);
  p =createP('Sets of feet '+slider.value())
  p.position((windowWidth/2)+150,windowHeight-65)
feetMaker()



let button=createButton('Restart')
button.position((windowWidth/2)-100,windowHeight-45)
button.size(150,40)
button.mousePressed(feetMaker);
}
function feetMaker(){
  feet=[];
  let nof = slider.value();
background(0);
  for (let i=0;i<nof;i++){
n1=0;
h1=10;
k1=0;
l1=10;
d1=1;
e1=0;
f1=0;

t1=random(0,100000);
u1=random(0,100000);

v1=createVector(0,0);
v2=createVector(0,0);


mr1=random(0,255);
mg1=random(0,255);
mb1=random(0,255);


//end of variable setup


//spawn points for every person


if(random(0,2)<1){
  x1=random(-100,windowWidth+100)
} else if(random(0,2)<1){
  x1=windowWidth+100;
} else{
  x1=-100;
}
if(x1>-100 && random(0,2)<1){
  y1=-100;
} else if(x1==-100||x1==windowWidth+100){
  y1=random(-100,windowHeight+100)
} else{
  y1=windowHeight+100
}
feet[i] = new Feet(a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,mr1,mg1,mb1,n1,o1,q1,t1,u1,v1,w1,x1,y1);
//End of spawn points
}
}



//the drawing

function draw() {
  p.html('Sets of feet '+slider.value())
push()
  background(0,0.5)


//lights and road
for(let x=0;x<windowHeight+100;x=x+750){
  fill(255,200,0,25)
  ellipse(350,x,400,400)
  ellipse(windowWidth-350,x,400,400)
}

for(let y=0;y<windowHeight+100;y=y+300){
rectMode(CENTER)
  fill(150,50)
  rect(300,y,20,300)
  rect(windowWidth-300,y,20,300)
}

for(let y=0;y<windowHeight+100;y=y+600){
rectMode(CENTER)
  fill(150,50)
  rect(windowWidth/2,y,20,300)
}
for (let i =feet.length-1;0<=i;i--){
feet[i].feetDraw();
}
}

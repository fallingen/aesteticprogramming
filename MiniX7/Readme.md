## MiniX7 submission
Walking feet revisited

-----

### Screen shots, executable link and code link  


Screenshot of program after about 10 seconds:<br>
https://gitlab.com/fallingen/aesteticprogramming/-/blob/main/MiniX7/MiniX7_1_feet.png

<br>

Executable program
https://fallingen.gitlab.io/aesteticprogramming/MiniX7/

<br>

code repository:

Original executable program:

https://fallingen.gitlab.io/aesteticprogramming/MiniX5/

original MiniX5 code:
https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX5

original MiniX5 testing code

https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX5/testMiniX5
 -----

 ### The rework thoughts
 <br>
So i decided to redo my MiniX5 program. I did this both based on the feedback i got from it and my own statement aobut it; there was just so much code. Turn out the week after we learn about objects and classes, and that owuld have shortened the code a lot. So now that there was a rework chance, i wanted to try and use that knowledge to make the code shorter, and use the benefits of the object system to add some user control.
 <br>
the original MiniX5 was 853 lines long, and a lot of it was just repeating code for each set of feet and clear space line inbetween so it was 'somewhat' manageable to look at
this new code is able to do more, and is only 216 lines of code, about 1/4 the size. most of the code changes was simply changing the variables to a this. version in the 2nd .js file, and then added the button and slider functionality.          
 <br>
 ### The new program, does it change?
<br>
The new program is a little bit different. first the shorter code length perhaps makes it slightly less confusing to read(though the about of variables still is a bit much), and perhaps more easily allows people to change stuff to try and figure out how everything works together. Second, the addition of the slider and reset button allows the auto generative content to take on some user input, while still being very much a auto generative program. What the user can change is the number of sets of feet that spawn in. The new program now allows someone to spawn 1 set of feet, where they can have a more focused tracking of just one object. Or they can try and turn it up tp 20 and see the chaos unfold infront of them. The ability to go lower on the feet count perhaps also allows lower end system to actually it, if i was computationally heavy before(though i don't know if it is/was).     
 <br>
And thanks again to Daniel Shiffman for this excellent video tutorial https://www.youtube.com/watch?v=587qclhguQg&t=0s

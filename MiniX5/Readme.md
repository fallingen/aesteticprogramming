## MiniX5 submission
Moodle Board

-----

### Screen shots, executable link and code link  


Screenshot of program after about 10 minutes:<br>
https://gitlab.com/fallingen/aesteticprogramming/-/blob/main/MiniX5/10_min_minix5.png

<br>

Executable program
https://fallingen.gitlab.io/aesteticprogramming/MiniX5/

<br>

code repository :
https://gitlab.com/fallingen/aesteticprogramming/-/tree/main/MiniX5


 -----

 ### Development thoughts
 <br>
So what i have created is a generative food traffic program that leaves the imprints of feet on the canvas. My initial idea when i read the MiniX5 was to generate something akin to what i did in Minix1, where i made a random spider generator. With Langton’s Ant in mind, i initially wanted to try and create tons of tiny spiders that would then move chaotically around on the screen. Though i went away from the spiders idea, and i then wanted to have another creature move across the screen, humans!. So the "simple" thing i wanted to have was a pair of feet outlines move across the canvas. Getting moving object across the screen was a fairly simple task    
 <br>

 Getting a moving object across the screen was a fairly simple task, the feet moving across screen could be achieved with similar code to a ball bouncing code. This next though was what might have lead me down a path of having to learn and write a lot of code. The basic bouncing code moves linearly, like the famous dvd screen saver. People usually move differently through space. So i wanted to ad a form of randomness to the feets movement, but also randomness that would make "sense" for "simulated" human behavior. And i had looked up the noise() syntax, which seemed to fit perfectly with the movement behavior i wanted. Figuring it out took some time, and my experimenting can be seen on the testMiniX5.js file, feel free to download the code and see the ellipses, points and line i drew in order to try and understand how to get the "walking simulation" i wanted. Some of the challenges that took some time to sort out was; rotating the feet in the direction on the movement(which meant getting a variable point use the translate syntax), jogling the amount of variables, making sure the movement and drawing was aligned and worked together etc. Getting those translate points was by far the toughest and i found a great amount of help in Daniel Shiffman's, aka. The Code Train, youtube guide called the nature of code, part 3.1-3.4 https://www.youtube.com/watch?v=DMg-WRfNB60        
 <br>
 ### Initial Rules and thoughts
<br>
At first i thought, "have feet that move across the canvas and leave imprints" was infact "simple" rules, turned out maybe not, at least not for my ability. Alas a program was made. Over time it changes in different ways. Early on in the program you'll see the sets of feet slowly start to take in some real estate on the road, with the lights, road markings and curb fighting back. As  a little time has passes almost the entire road had been filled with foot prints and, at this point the program stop evolving much more, even though new random coloured once will continue to be put down. what you'll see the clearest anytime is the somewhat diagonal patters left behind like "chem trail" left amongst the other clouds. The over all diagonal behavior comes from the fact that both the x and y values changes each frame, and they both keep going in the same directing until just beyond the window border. And the added chaos from the noises() syntax is apparently kinda binomial in distribution, so since both movement have the same random ranges, this is to be expected.      
 <br>
